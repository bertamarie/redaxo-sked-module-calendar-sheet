# redaxo-sked-module-calendar-sheet

This is a frontend solution for redaxos calendar plugin - "sked".
It provides a calendar sheet with weekly and monthly view and clickable events for further information.

- shows event pictures
- event dates and multiday events
- event times
- linked files
- teaser text

didn't implement skeds text field

My special thanks to https://github.com/javanita who helped getting started and provided the php code and some of js code, too.

For use pleas read how to file. You can also view screenshots.